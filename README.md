
#This is an implementation of the NEB algorithm that runs over sockets.


##To run this code 

###Download the code and start the wbwserver
 1.	git clone https://ebylaska@bitbucket.org/ebylaska/python-neb.git
 2. edit wbwserver6_nwchem to change the location of a nwchem binary on your machine.
 3.	cd python-neb
 4.	mkdir JUNK
 5.	cd JUNK
 6.	../ wbwserver6_nwchem start
 
###Open another terminal on your machine
 1.	cd python-neb
 2. mkdir RUN1
 3. cd RUN1
 4. python ../neb.py ../clch3cl-neb.nw
 
###Open another terminal on your machine
 1. cd python-neb
 2. mkdir RUN2
 3. cd RUN2
 4. python ../neb.py ../sih4-neb.nw
 
###When done running NEB calculations
 1. cd python-neb
 1. wbwserver6_nwchem stop





