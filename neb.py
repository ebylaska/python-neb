#!/usr/local/bin/python
#### neb.py

####  The server and neb programs were written by
####
####     Eric J. Bylaska 
####     Environmental Molecular Sciences Laboratory
####     Pacific Northwest National Laboratory
####     Richland, Washington 99354
####
####


import sys,os,time,socket,pickle,math,xyplotter
import numpy as np

#### atomic symbols ####
def_symbols = ['H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 'Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I', 'Xe', 'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn', 'Fr', 'Ra', 'Ac', 'Th', 'Pa', 'U ', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es', 'Fm', 'Md', 'No', 'Lr', 'Rf', 'Db', 'Sg', 'Bh', 'Hs', 'Mt']
#### atomic masses ####
def_masses = [1.007825, 4.00260, 7.0160, 9.01218, 11.00931, 12.0, 14.00307, 15.99491, 18.9984, 19.99244, 22.9898, 23.98504, 26.98154, 27.97693, 30.97376, 31.97207, 34.96885, 39.9624, 38.96371, 39.96259, 44.95592, 45.948, 50.9440, 51.9405, 54.9381, 55.9349, 58.9332, 57.9353, 62.9298, 63.9291, 68.9257, 73.9219, 74.9216, 79.9165, 78.9183, 83.912, 84.9117, 87.9056, 88.9054, 89.9043, 92.9060, 97.9055, 97.9072, 101.9037, 102.9048, 105.9032, 106.90509, 113.9036, 114.9041, 117.9018, 120.9038, 129.9067, 126.9004, 131.9042, 132.9051, 137.9050, 138.9061, 139.9053, 140.9074, 143.9099, 144.9128, 151.9195, 152.9209, 157.9241, 159.9250, 163.9288, 164.9303, 165.9304, 168.9344, 173.9390, 174.9409, 179.9468, 180.948,  183.9510, 186.9560, 189.9586, 192.9633, 194.9648, 196.9666, 201.9706, 204.9745, 207.9766, 208.9804, 209.9829, 210.9875, 222.0175, 223.0198, 226.0254, 227.0278, 232.0382, 231.0359, 238.0508, 237.0482, 244.0642, 243.0614, 247.0704, 247.0703, 251.0796, 252.0829, 257.0950, 258.0986, 259.1009, 262.1100, 261.1087, 262.1138, 266.1219, 262.1229, 267.1318, 268.1388 ]


#############################################
#                                           #
#             my_sockconnect                #
#                                           #
#############################################
#
# Returns connected socket to hostport or None if failed
# where hostport = "ipaddress:port" (e.g. hostport="localhost:50001")
#
def my_sockconnect(hostport):
   l = hostport.index(':'); r = hostport.rindex(':')
   if (r==l): hp = (hostport[:l],eval(hostport[l+1:]))
   else:      hp = (hostport[:l],eval(hostport[l+1:r]))
   s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
   s.settimeout(10.0)
   try:
      s.connect(hp)
      s.settimeout(None)
      return s
   except:
       s.close()
       s = None
   return s



#######################################################
#                                                     #
#                   runcalcs                          #
#                                                     #
#######################################################
#
# This routine calculates the energy-gradient using the dictionary job
# of the  list of xs geometries.
#
def runcalcs(hostsports,job,xs,mstart=None):
   if (mstart==None): mstart = 0
   egrads = []
   jobs = []
   for i in range(mstart,len(xs)):
      jobs.append(job.copy())
      jobs[i-mstart]['xyz'] = xs[i]
      jobs[i-mstart]['jobnumber'] += i
   njobs = len(jobs); nhp = len(hostsports)

   ## set up connections ###
   ss = []; sscpus = []; ncpus  = 0
   for hp in hostsports[:njobs]:
      s = my_sockconnect(hp)
      if (s!=None):
         ss.append(s)
         l = hp.index(':'); r = hp.rindex(':')
         if (l==r): cpus = 1
         else:      cpus = eval(hp[r+1:])
         sscpus.append(cpus); ncpus += cpus
   if (len(ss)>0):
      ns = len(ss)
      jobcount = [0]*ns
      for i in range(ns):                  jobcount[i]     = int(njobs*sscpus[i]/float(ncpus))
      for i in range(njobs-sum(jobcount)): jobcount[i%ns] += 1
      ## send jobs to connections ##
      i = -1; istart = 0
      for s in ss:
         iend = istart + jobcount[i+1]
         s.sendall(pickle.dumps(jobs[istart:iend])+'ericdone')
         istart = iend
         i += 1
      for s in ss:
         msg = ''
         done = False
         while not done:
            data = s.recv(1024)
            msg  =  msg + data
            done = msg[len(msg)-8:len(msg)] == 'ericdone'
         msg  = msg[:len(msg)-8]
         data = pickle.loads(msg)
         egrads += data
         s.close()

   return egrads



#############################################
#                                           #
#                 util_date                 #
#                                           #
#############################################
def util_date():
   tt      = time.localtime()
   dd = "%d-%d-%d-%d:%d" % (tt[0],tt[1],tt[2],tt[3],tt[4])
   return dd




#######################################################
#                                                     #
#                   QHess3 class                      #
#                                                     #
#######################################################
#
# This class implements a Broyden quasi-Newton algorithm
#
class QHess3:
   m=0
   im=0
   mmax = 0
   c  = []
   g  = []
   x  = []
   y  = []
   Hy = []
   a  = []
   def __init__(self, n0, mmax0=5):
      self.n = n0
      self.mmax = mmax0
      self.m  = 0
      self.im = 0
      self.c = [0.0]*self.mmax*self.n
      self.g = [0.0]*self.mmax*self.n
      self.x = [0.0]*self.mmax*self.n
      self.y = [0.0]*self.mmax*self.n
      self.Hy = [0.0]*self.mmax*self.n
      self.a  = [0.0]*self.mmax

   def reset(self):
      self.m  = 0
      self.im = 0
      self.c = [0.0]*self.mmax*self.n
      self.g = [0.0]*self.mmax*self.n
      self.x = [0.0]*self.mmax*self.n
      self.y = [0.0]*self.mmax*self.n
      self.Hy = [0.0]*self.mmax*self.n
      self.a  = [0.0]*self.mmax

   def size(self): return self.m

   def AddHistory(self,c1,g1):
      s  = ((self.im+self.m)  %self.mmax)*self.n
      sm = ((self.im+self.m-1)%self.mmax)*self.n
      for i in range(self.n):
         self.c[s+i] = c1[i]
         self.g[s+i] = -g1[i]
      if (self.m>0):
         for i in range(self.n):
            self.x[sm+i] = self.c[s+i]-self.c[sm+i]
            self.y[sm+i] = self.g[s+i]-self.g[sm+i]
         for i in range(self.n):
            self.Hy[sm+i] = self.y[sm+i]

         for k in range(self.m-1):
            kkm = (k+self.im)%self.mmax
            km  = kkm*self.n
            b = 0.0
            for i in range(self.n): b += self.x[km+i]*self.Hy[sm+i]
            for i in range(self.n): self.Hy[sm+i] += b*(self.x[km+i] + self.Hy[km+i])/self.a[kkm]
         mm = (self.im+self.m-1)%self.mmax
         aa = 0.0
         for i in range(self.n): aa += self.x[sm+i]*self.Hy[sm+i]
         self.a[mm] = aa
      if (self.m>=(self.mmax-1)):
         self.im += 1
      else:
         self.m += 1

   def HBroyden(self,f):
      HBf = [0.0]*self.n
      for i in range(self.n): HBf[i] = f[i]
      for k in range(self.m-1):
         kkm = (k+self.im)%self.mmax
         km  = kkm*self.n
         b = 0.0
         for i in range(self.n): b = self.x[km+i]*f[i]
         for i in range(self.n):
            HBf[i] += b*(self.x[km+i] + self.Hy[km+i])/self.a[kkm]

      return HBf




##############################################
#                                            #
#             plot_nebresidual               #
#                                            #
##############################################
def plot_nebresidual(plot,nion3,nbeads,R):
   #### plot residual error ####
   ymin =  9.999e+99
   ymax = -9.999e+99
   rr = []
   for p in range(nbeads):
      y = 0.0
      for j in range(nion3):
         y += R[j+p*nion3]*R[j+p*nion3]
      y = math.sqrt(y)
      rr.append(y)
      if (y>ymax): ymax = y
      if (y<ymin): ymin = y
   ymin = 0.0
   plot.resetwindow(0.0,ymin,1.0,ymax,"NEB Residual Errors")
   plot.plot1(rr,"purple")

##############################################
#                                            #
#             plot_nebpaths                  #
#                                            #
##############################################
def plot_nebpaths(plot,neb_pathsenergies):
   colors = ("blue","green","yellow","purple","orange")
   ncc    = len(colors)
   delta = 0.0
   pfile = open(neb_pathsenergies,'r')
   yplot = []
   y = []
   pcc=0; pc=0;
   y0 = None
   ymax = -999999.9e+99
   ymin = +999999.9e+99
   for line in pfile:
      ss = line.split()
      if (len(ss)>1):
         if (y0 is None): y0 = eval(ss[1])
         y1 = (eval(ss[1]) - y0)*27.2116*23.06
         y.append(y1)
         if (y1>ymax): ymax = y1
         if (y1<ymin): ymin = y1
      else:
         yplot.append([y,colors[pc]])
         y = []
         pcc += 1
         if ((pcc%ncc)==0): pc = (pc+1)%ncc
   pfile.close()
   plot.resetwindow(0.0,ymin-delta,1.0,ymax+delta,"neb_relax Plot")
   for yy in yplot: plot.plot1(yy[0],yy[1])


##############################################
#                                            #
#             neb_set_initial_final          #
#                                            #
##############################################
def neb_set_initial_final(hostsports,nwjob,c0,cf):
   global c0f,egrads0f
   nion3 = len(c0)
   c0f = []
   c0f.append([])
   for j in range(nion3):
      c0f[0].append(c0[j])
   c0f.append([])
   for j in range(nion3):
      c0f[1].append(cf[j])
   egrads0f = runcalcs(hostsports,nwjob,c0f)

##############################################
#                                            #
#         neb_get_initial_final_energies     #
#                                            #
##############################################
def neb_get_initial_final_energies():
   global c0f,egrads0f
   return (egrads0f[0][0],egrads0f[1][0])



##############################################
#                                            #
#             neb_restriction                #
#                                            #
##############################################

def neb_restriction(nion3,l,Xfine,Xcoarse):
   nglm = 2**(l)+1
   for p in range(nglm):
      for j in range(nion3):
         Xcoarse[j+p*nion3] =  Xfine[j+2*p*nion3]

##############################################
#                                            #
#             neb_prolongation               #
#                                            #
##############################################

def neb_prolongation(nion3,l,Xcoarse,Xfine):
   ngl  = 2**(l+1)+1
   for p in range(ngl):
      for j in range(nion3):
         Xfine[j+2*p*nion3] =  Xcoarse[j+p*nion3]

   for p in range(ngl-1):
      for j in range(nion3):
         Xfine[j+(2*p+1)*nion3] =  0.5*(Xcoarse[j+p*nion3] + Xcoarse[j+(p+1)*nion3])

##############################################
#                                            #
#             neb_generate_X                 #
#                                            #
##############################################

def neb_generate_X(nion3,lmax):
   X0 = []
   for l in range(lmax+1):
      X0.append([])
      for ii in range(nion3*(2**(l+1)+1)):
         X0[l].append(0.0)
   return X0


##############################################
#                                            #
#             neb_tangent0                    #
#                                            #
##############################################
def neb_tangent0(nion3,nbeads,e,c):
   tall = [0.0]*nion3
   for p in range(1,nbeads-1):
      tm = []; tp = []; t=[]
      for j in range(nion3):
         tm.append(c[j+p*nion3]-c[j+(p-1)*nion3])
         tp.append(c[j+(p+1)*nion3]-c[j+p*nion3])
         t.append(0.0)
      normm = 1.0/math.sqrt(sum([x*x  for x in tm]))
      normp = 1.0/math.sqrt(sum([x*x  for x in tp]))
      for j in range(nion3):
         t[j] = normm*tm[j] + normp*tp[j]

      ### normalize the tangent ####
      norm = sum([x*x  for x in t])
      norm = 1.0/math.sqrt(norm)
      for j in range(nion3): t[j] *= norm
      tall = tall + t

   tall = tall + [0.0]*nion3
   return tall

##############################################
#                                            #
#             neb_tangent                    #
#                                            #
##############################################
def neb_tangent(nion3,nbeads,e,c):
   tall = [0.0]*nion3
   for p in range(1,nbeads-1):
      tm = []; tp = []; t=[]
      for j in range(nion3):
         tm.append(c[j+p*nion3]-c[j+(p-1)*nion3])
         tp.append(c[j+(p+1)*nion3]-c[j+p*nion3])
         t.append(0.0)
      if ( (e[p+1]>e[p]) and (e[p]>e[p-1])):
         for j in range(nion3): t[j] = tp[j]
      elif ( (e[p-1]>e[p]) and (e[p]>e[p+1]) ):
         for j in range(nion3): t[j] = tm[j]
      else:
         dVmax = max(abs(e[p+1]-e[p]), abs(e[p-1]-e[p]))
         dVmin = min(abs(e[p+1]-e[p]), abs(e[p-1]-e[p]))
         if (e[p+1]>e[p-1]):
            for j in range(nion3): 
               t[j] = tp[j]*dVmax + tm[j]*dVmin
         else:
            for j in range(nion3): 
               t[j] = tp[j]*dVmin + tm[j]*dVmax

      ### normalize the tangent ####
      norm = sum([x*x  for x in t])
      norm = 1.0/math.sqrt(norm)
      for j in range(nion3): t[j] *= norm
      tall = tall + t

   tall = tall + [0.0]*nion3
   return tall

##############################################
#                                            #
#             neb_project_tangent            #
#                                            #
##############################################
def neb_project_tangent(nion3,nbeads,t,g0):
   gall = [0.0]*nion3
   for p in range(1,nbeads-1):
      norm = sum([g0[i+p*nion3]*t[i+p*nion3] for i in range(nion3)])
      g = [g0[i+p*nion3] - norm*t[i+p*nion3] for i in range(nion3)]
      gall = gall + g
   gall = gall + [0.0]*nion3
   return gall
      

##############################################
#                                            #
#             neb_spring                     #
#                                            #
##############################################
#
#returns spring_force
#
def neb_spring(nion3,nbeads,Kspring,c,t):
   gsall = [0.0]*nion3
   for p in range(1,nbeads-1):
      tm = [c[i+p*nion3]-c[i+(p-1)*nion3] for i in range(nion3)]  
      tp = [c[i+(p+1)*nion3]-c[i+p*nion3] for i in range(nion3)]  
      normm = sum([x*x for x in tm])
      normp = sum([x*x for x in tp])
      norm = Kspring*(math.sqrt(normp) - math.sqrt(normm))
      gs = [t[i+p*nion3]*norm for i in range(nion3)]

      gsall = gsall + gs
   gsall = gsall + [0.0]*nion3

   return gsall

##############################################
#                                            #
#             neb_gradient                   #
#                                            #
##############################################
#
#returns (|gradient|,path_energies,gradient) where gradient = -Fneb = F(c)
#
def neb_gradient(hostsports,nwjob,nion3,nbeads,Kspring,c):
   (e0,ef) = neb_get_initial_final_energies()
   c0f = []
   for p in range(1,nbeads-1):
      c0f.append([])
      for j in range(nion3):
         c0f[p-1].append(c[j+p*nion3])
   egrads = runcalcs(hostsports,nwjob,c0f)

   e = [e0] + [p[0] for p in egrads] + [ef]
   g = [0.0]*nion3
   for p in egrads:
      g = g + p[1]
   g = g + [0.0]*nion3
   
   #t = neb_tangent0(nion3,nbeads,e,c)
   t = neb_tangent(nion3,nbeads,e,c)
   gneb  = neb_project_tangent(nion3,nbeads,t,g)
   gs    = neb_spring(nion3,nbeads,Kspring,c,t)

   for p in range(nbeads):
      for j in range(nion3):
         gneb[j+p*nion3] += gs[j+p*nion3]

   ### multiply by -1 to make it a gradient rather than a force ###
   for p in range(nbeads):
      for j in range(nion3):
         gneb[j+p*nion3] *= -1.0

   norm = 0.0
   for p in range(nbeads):
      for j in range(nion3):
         norm += gneb[j+p*nion3]*gneb[j+p*nion3]
   norm = math.sqrt(norm)

   return (norm,e,gneb)


##############################################
#                                            #
#             neb_initialize                 #
#                                            #
##############################################
def neb_initialize(lmax,timestep0):
   global manyjj, goodjj, timestep
   manyjj   = [0]*(lmax+1)
   goodjj   = [0]*(lmax+1)
   timestep = [timestep0]*(lmax+1)


##############################################
#                                            #
#             neb_freeze_init                #
#                                            #
##############################################
def neb_freeze_init(lmax):
   global frozen_beads
   frozen_beads = []
   for l in range(lmax+1):
      frozen_beads.append([])

##############################################
#                                            #
#             neb_freeze_set                 #
#                                            #
##############################################
def neb_freeze_set(l,beads):
   global frozen_beads
   if (len(beads)==0):
      frozen_beads[l] += []
   else:
      frozen_beads[l] += beads

##############################################
#                                            #
#             neb_freeze                     #
#                                            #
##############################################
def neb_freeze(l,nion3,nbeads,g):
   global frozen_beads
   for b in frozen_beads[l]:
      for i in range(nion3):
         g[i+b*nion3] = 0.0


##############################################
#                                            #
#             neb_relax                      #
#                                            #
##############################################
#
#returns (|residual|,path_energies,residual)
#
def neb_relax(hostsports,nwjob,nrelax,timestep0,nion3,nbeads,l,Kspring,c,sneb):
   global manyjj, goodjj, timestep
  
   c0 = [0.0]*nion3*nbeads

   ### compute the initial neb residual ###
   (error,e,gneb) = neb_gradient(hostsports,nwjob,nion3,nbeads,Kspring,c)
   for i in range(nion3*nbeads): gneb[i] = sneb[i] - gneb[i]
   neb_freeze(l,nion3,nbeads,gneb)
   sum0 = 0.0
   for i in range(nion3*nbeads): sum0 += gneb[i]*gneb[i]

   ### initialize the hessian ###
   nhist = nrelax + 1
   if (nhist<5):  nhist = 5
   if (nhist>15): nhist = 15
   qall = QHess3(nion3*nbeads,nhist)

   ### run nrelax iterations ###
   for it in range(nrelax):

      sum0_old = sum0

      ### compute the current direction HBf ###
      qall.AddHistory(c,gneb)
      HBf = qall.HBroyden(gneb)
      sum1 = 0.0
      for i in range(nbeads*nion3): sum1 += HBf[i]*gneb[i]
      if (sum1<=0.0):
         print "wrong direction sum1=",sum1
         for i in range(nbeads*nion3): HBf[i] = gneb[i]
         qall.reset()

      #for i in range(nbeads*nion3):HBf[i] = gneb[i]


      ### do a simple line search ###
      for i in range(nion3*nbeads): c0[i] = c[i]
      alpha = timestep[l]
      finished_step = False
      jj = 0
      while ((not finished_step) and (jj<9)):
         jj += 1

         for i in range(nion3*nbeads): c[i] = c0[i] + alpha*HBf[i]

         ### compute the current residual  ###
         (error,e,gneb) = neb_gradient(hostsports,nwjob,nion3,nbeads,Kspring,c)
         for i in range(nion3*nbeads): gneb[i] = sneb[i] - gneb[i]
         neb_freeze(l,nion3,nbeads,gneb)

         sum0 = 0.0
         for i in range(nion3*nbeads): sum0 += gneb[i]*gneb[i]

         finished_step = (sum0<=sum0_old)
         if (not finished_step):
            alpha *= 0.5
            manyjj[l] += 1
            goodjj[l] = 0
            if (manyjj[l]>2):
               timestep[l] *= 0.5
               manyjj[l] = 0
         elif (jj==1):
            goodjj[l] += 1
            if (goodjj[l]>9):
               goodjj[l] = 0
               manyjj[l] = 0
               timestep[l] *= 2.0
               if (timestep[l]>timestep0): timestep[l] = timestep0
         
      print "  --> neb_relax nbeads,it,jj,norm,norm_old,timestep=",nbeads,it,jj,math.sqrt(sum0),math.sqrt(sum0_old),timestep[l]
      ### did not finish line search, reduce time step  and reset jacobian ###
      if (not finished_step):
         timestep[l] *= 0.5
         qall.reset()

   #### timestep is really low, look for a divding point ####
   if (timestep[l]<1.0e-4):
      ymax = -9.999e99
      bzero = 0
      for p in range(nbeads):
         y = 0.0
         for j in range(nion3):
            y += gneb[j+p*nion3]*gneb[j+p*nion3]
         if (y>ymax):
            ymax = y
            bzero = p
      timestep[l] = timestep0
      manyjj[l] = 0
      goodjj[l] = 0
      if (bzero==0):
         neb_freeze_set(l,[bzero,bzero+1])
      elif (bzero==(nbeads-1)):
         neb_freeze_set(l,[bzero-1,bzero])
      else:
         neb_freeze_set(l,[bzero-1,bzero,bzero+1])
      print "dividing surface set at l=",l," and bead=",bzero

   norm = math.sqrt(sum0)
   return (norm,e,gneb)


##############################################
#                                            #
#             neb_vcycle_relax               #
#                                            #
##############################################
#
#  This function does one FAC Vcycle
#
#  Entry - hostports: define hosts and ports for doing calculations
#          nwjobs: dictionary (json) describing nwchem job
#          Nrelax: number of iterations in underlying root finder
#          timestep: suggested time step in underlying root finder
#          nion3: 3*nion
#          lmax:  number of levels
#          Kspring: NEB spring constant for lmax level
#          X: lmax+1 levels of grids
#
#  Exit - returns residual error for X(lmax) and current list of neb energies
#
def neb_vcycle_relax(hostsports,nwjob,Nrelax,timestep,nion3,lmax,Kspring,X):

   ### Allocate lmax+1 levels of grids ###
   X0 = neb_generate_X(nion3,lmax)
   S0 = neb_generate_X(nion3,lmax)
   R0 = neb_generate_X(nion3,lmax)
   E0 = neb_generate_X(nion3,lmax)

   #########################
   ### 1 Vcycle iteraton ###
   #########################
   K = Kspring

   ### looping from fine grids to coarse grids ###
   ### note that spring constant is halved at each restriction ###
   for l in range(lmax,0,-1):
      nbeads  =  2**(l+1)+1
      nbeadsm =  2**(l)+1

      ### approximate  F(X(l)) = S0(l) for X(l) and return the residual R = S0(l)-F(X(l)) ####
      ### Note that S0(lmax) = 0 ###
      (error,pathenergies,R) = neb_relax(hostsports,nwjob,Nrelax,timestep,nion3,nbeads,l,K,X[l],S0[l])

      ### Restrict R and X ###
      neb_restriction(nion3,l,R,R0[l-1])
      neb_restriction(nion3,l,X[l],X[l-1])
      neb_restriction(nion3,l,X[l],X0[l-1])

      ### solve for F(X(l-1) and then define S0(l-1) = F(X(l-1)) + R0(l-1) ###
      ### note that spring constant is halved at each restriction ###
      K = 0.5*K
      (error0,e,F) = neb_gradient(hostsports,nwjob,nion3,nbeadsm,K,X0[l-1])
      for i in range(nion3*nbeadsm): S0[l-1][i] = F[i] + R0[l-1][i]
      neb_freeze(l-1,nion3,nbeadsm,S0[l-1])
    
   ### approximate  F(X(l=0)) = S0(l=0) for X(l=0) and return the residual R = S0(l=0)-F(X(l=0)) ####
   nbeads = 3
   (error,pathenergies,R) = neb_relax(hostsports,nwjob,Nrelax,timestep,nion3,nbeads,0,K,X[0],S0[0])

   ### loop to propagate error back to fine grids ###
   ### note that spring constant is doubled at each prolongation ###
   for l in range(lmax):
      nbeads  =  2**(l+1)+1
      nbeadsp =  2**(l+2)+1

      ### prolonate X(l)-X0(l) to coarse grid, E0(l+1) = Prolongate(X(l)-X0(l)) ###
      for i in range(nion3*nbeads): E0[l][i] = X[l][i] - X0[l][i]
      neb_prolongation(nion3,l,E0[l],E0[l+1])

      ### correct X(l+1) = X(l+1) + E0(l+1) ###
      neb_freeze(l+1,nion3,nbeadsp,E0[l+1])
      for i in range(nion3*nbeadsp): X[l+1][i] = X[l+1][i] + E0[l+1][i]

      ### approximate  F(X(l+1)) = S0(l+1) for X(l+1) and return the residual R = S0(l+1)-F(X(l+1)) ####
      ### note that spring constant is doubled at each prolongation ###
      K = 2.0*K
      (error,pathenergies,R) = neb_relax(hostsports,nwjob,Nrelax,timestep,nion3,nbeadsp,l+1,K,X[l+1],S0[l+1])
 
   return (error,pathenergies,R)


###########################################################################
########################### main program ##################################
###########################################################################

def main():
   import sys,os,getopt


   usage = \
   """
  This program runs and NEB calculation

  Usage: neb -t timestep -l lmax -d  input.nw
  
   where nbeads is the number of beads

   """

   debug = False
   opts, args = getopt.getopt(sys.argv[1:], "t:l:dh")
   for o, a in opts:
      if '-t' in o:
         timestep = eval(a)
      if '-l' in o:
         lmax = eval(a)
      if '-d' in o:
         debug = True
      if o in ("-h","--help"):
         print usage
         exit()

   if (len(args)<1):
      print "\nEnter nwchem input file:"
      nwfilename = sys.stdin.readline()[:-1]
   else:
      nwfilename = args[0]

   ###### read nwchemfilename ######
   nwfile = open(nwfilename,'r')
   nwstring = nwfile.read()
   nwfile.close()

   ## timestep ##
   ts = nwstring.find('neb_timestep')
   te = ts + nwstring[ts:].find('\n')
   if ((ts != -1) and (te != -1)):
      line = nwstring[ts:te].split()
      timestep = eval(line[1])
   else:
      timestep = 0.1

   ## residerr ##
   ts = nwstring.find('neb_residerr')
   te = ts + nwstring[ts:].find('\n')
   if ((ts != -1) and (te != -1)):
      line = nwstring[ts:te].split()
      residerr = eval(line[1])
   else:
      residerr = 1.0e-4

   ## Nsweeps ##
   ts = nwstring.find('neb_Nsweeps')
   te = ts + nwstring[ts:].find('\n')
   if ((ts != -1) and (te != -1)):
      line = nwstring[ts:te].split()
      Nsweeps = eval(line[1])
   else:
      Nsweeps = 15

   ## charge ##
   ts = nwstring.find('charge')
   te = ts + nwstring[ts:].find('\n')
   if ((ts != -1) and (te != -1)):
      line = nwstring[ts:te].split()
      charge = eval(line[1])
   else:
      charge = 0

   ## Nrelax ##
   ts = nwstring.find('neb_Nrelax')
   te = ts + nwstring[ts:].find('\n')
   if ((ts != -1) and (te != -1)):
      line = nwstring[ts:te].split()
      Nrelax = eval(line[1])
   else:
      Nrelax = 3

   ## lmax ##
   ts = nwstring.find('neb_lmax')
   te = ts + nwstring[ts:].find('\n')
   if ((ts != -1) and (te != -1)):
      line = nwstring[ts:te].split()
      lmax = eval(line[1])
   else:
      lmax = 2

   ## Kspring ##
   ts = nwstring.find('neb_Kspring')
   te = ts + nwstring[ts:].find('\n')
   if ((ts != -1) and (te != -1)):
      line = nwstring[ts:te].split()
      Kspring = eval(line[1])
   else:
      Kspring = 0.1

   ## Vcycle ##
   ts = nwstring.find('neb_Vcycle')
   if (ts != -1):
      Vcycle = True
   else:
      Vcycle = False

   ## neb_paths_energies ##
   ts = nwstring.find('neb_paths_energies')
   te = ts + nwstring[ts:].find('\n')
   if ((ts != -1) and (te != -1)):
      line = nwstring[ts:te].split()
      neb_pathsenergies = line[1]
   else:
      neb_pathsenergies = "neb_paths_energies.dat"

   ## hostsports ##
   ts = nwstring.find('neb_hostsports')
   te = ts + nwstring[ts:].find('\n')
   if ((ts != -1) and (te != -1)):
      t1 = ts + nwstring[ts:te].find('[')
      t2 = ts + nwstring[ts:te].find(']')
      hostsports  = eval(nwstring[t1:t2+1])
   else:
      hostsports  = ['localhost:50001:2']


   ## nwtheory ##
   ts = nwstring.find('neb_theory_start')
   te = nwstring.find('neb_theory_end')
   if ((ts != -1) and (te != -1)):
      t1 = ts + nwstring[ts:te].find('\n')
      t2 = ts + nwstring[ts:te].rfind('\n')
      nwtheory = nwstring[t1+1:t2]
   else:
      nwtheory = "nwpw\n"
      nwtheory += "end\n"
      nwtheory += "task pspw gradient\n"

   ## xyz trajectory ##
   ts = nwstring.find('neb_path_filename')
   te = ts + nwstring[ts:].find('\n')
   if ((ts != -1) and (te != -1)):
      line = nwstring[ts:te].split()
      xyzfilename2 = line[1]
      if (os.path.isfile(xyzfilename2)):
         readpathxyz = True
      else:
         readpathxyz = False
   else:
      xyzfilename2 = "extrapolated path from nwfile"
      readpathxyz = False

   if (readpathxyz): 
      pathxyz = [] 
      xyzfile = open(xyzfilename2,'r')
      c = xyzfile.readline().strip()
      while (c.isdigit()):
         nion = eval(c)
         symbols = []; x0 = []; v0 = []; mass = []
         xyzfile.readline()
         for ii in range(nion):
            line = xyzfile.readline().split()
            sym = line[0].lower().capitalize(); symbols.append(sym)
            i = def_symbols.index(sym); mass.append(def_masses[i]*1822.89)
            x0.append(eval(line[1])/0.529177); x0.append(eval(line[2])/0.529177); x0.append(eval(line[3])/0.529177)
            if (len(line)==7): v0.append(eval(line[4])/0.529177); v0.append(eval(line[5])/0.529177); v0.append(eval(line[6])/0.529177)
            else: v0.append(0.0); v0.append(0.0); v0.append(0.0)
         c = xyzfile.readline().strip()
         pathxyz.append(x0)
      xyzfile.close()

## read from nwfile ##
   else:

      #### read initial geom ####
      t1 = nwstring.find('geometry')
      t2 = t1 + nwstring[t1:].find('\n')
      t3 = t2 + nwstring[t2:].find('end')
      nion = nwstring[t2+1:t3].count('\n')

      uconv = 0.52917715
      if (nwstring[t1:t2].find('units bo') != -1): uconv = 1.0
      if (nwstring[t1:t2].find('units au') != -1): uconv = 1.0
      if (nwstring[t1:t2].find('units nm') != -1): uconv = 0.052917715
      if (nwstring[t1:t2].find('units na') != -1): uconv = 0.052917715
      if (nwstring[t1:t2].find('units pm') != -1): uconv = 52.917715
      if (nwstring[t1:t2].find('units pi') != -1): uconv = 52.917715

      symbols = []; x0 = []; v0 = []; mass = []
      ts = t2+1
      for ii in range(nion):
         te = ts + nwstring[ts:].find('\n')
         line = nwstring[ts:te].split()
         sym = line[0].lower().capitalize()
         symbols.append(sym)
         i = def_symbols.index(sym); mass.append(def_masses[i]*1822.89)
         x0.append(eval(line[1])/uconv); x0.append(eval(line[2])/uconv); x0.append(eval(line[3])/uconv)
         if (len(line)==7):
            v0.append(eval(line[4])/uconv); v0.append(eval(line[5])/uconv); v0.append(eval(line[6])/uconv)
         else:
            v0.append(0.0); v0.append(0.0); v0.append(0.0)
         ts = te+1


      #### read endgeom ####
      t1 = nwstring.find('geometry endgeom')
      t2 = t1 + nwstring[t1:].find('\n')
      t3 = t2 + nwstring[t2:].find('end')
      nion = nwstring[t2+1:t3].count('\n')

      uconv = 0.52917715
      if (nwstring[t1:t2].find('units bo') != -1): uconv = 1.0
      if (nwstring[t1:t2].find('units au') != -1): uconv = 1.0
      if (nwstring[t1:t2].find('units nm') != -1): uconv = 0.052917715
      if (nwstring[t1:t2].find('units na') != -1): uconv = 0.052917715
      if (nwstring[t1:t2].find('units pm') != -1): uconv = 52.917715
      if (nwstring[t1:t2].find('units pi') != -1): uconv = 52.917715

      symbols = []; x1 = []; v1 = []; mass = []
      ts = t2+1
      for ii in range(nion):
         te = ts + nwstring[ts:].find('\n')
         line = nwstring[ts:te].split()
         sym = line[0].lower().capitalize()
         symbols.append(sym)
         i = def_symbols.index(sym); mass.append(def_masses[i]*1822.89)
         x1.append(eval(line[1])/uconv); x1.append(eval(line[2])/uconv); x1.append(eval(line[3])/uconv)
         if (len(line)==7):
            v1.append(eval(line[4])/uconv); v1.append(eval(line[5])/uconv); v1.append(eval(line[6])/uconv)
         else:
            v1.append(0.0); v1.append(0.0); v1.append(0.0)
         ts = te+1

      #### set up path ####
      pathxyz = []
      pmax = 2**(lmax+1)
      for p in range(2**(lmax+1)+1):
         t = float(p)/float(pmax)
         pathxyz.append([])
         for ii in range(3*nion):
            pathxyz[p].append(x0[ii] + t*(x1[ii]-x0[ii]))


   nwjob = {}
   nwjob['symbols'] = symbols
   nwjob['nion'] = nion
   nwjob['charge'] = charge
   nwjob['theory']      = 'nwchemblock'
   nwjob['nwchemblock'] =  nwtheory
   nwjob['jobnumber'] = 0


   print
   print "          ****************************************************"
   print "          *                                                  *"
   print "          *         Nudged Elastic Band Calculation          *"
   print "          *                                                  *"
   print "          *     [      Full Approximation Scheme      ]      *"
   print "          *     [     Master/Slave Parallelization    ]      *"
   print "          *     [     Python Socket Implementation    ]      *"
   print "          *                                                  *"
   print "          *            version #1.00   07/16/2015            *"
   print "          *                                                  *"
   print "          ****************************************************"
   print "          >>> job started at       ", util_date()," <<<"
   print
   print "number of HOSTSPORTS =",len(hostsports)
   print "HOSTSPORTS =",hostsports
   print
   print "NEB iteration: Quasi-Newton V-Cycle"
   print
   print "########## neb_theory_start ##########"
   print nwtheory
   print "########## neb_theory_end   ##########"
   print
   print "charge                           =", charge
   print "number of atoms                  =", nion
   print "number of NEB beads              =", 2**(lmax+1)+1
   print "lmax                             =", lmax
   print "Kspring                          =", Kspring
   print "timestep                         =", timestep
   print "Maxinum number of Sweeps         =", Nsweeps
   print "Maxinum number of relaxations    =", Nrelax
   print "maximum residual error           =", residerr
   print "Vcycle on                        =", Vcycle

   if (readpathxyz):
      print "initial xyz path filename        = ",xyzfilename2
   else:
      print "initial xyz path filename        =  reading path from nwfile"
   print "trajectory xyz filename          = ",xyzfilename2
   print "neb_paths_energies filename      = ",neb_pathsenergies
   print
   if (debug):
      print "Intitial Geometry Path (a.u.):"
      for p in range(2**(lmax+1)+1):
         print "\ngeometry #%d" % p
         x = pathxyz[p]
         for ii in range(nion):
            print '%d  %s  %8.4f %8.4f %8.4f - mass = %8.2f' % (ii+1,nwjob['symbols'][ii],x[3*ii],x[3*ii+1],x[3*ii+2],mass[ii]/1822.89)
      print
      print "Intitial Geometry XYZ: %d Beads" % (2**(lmax+1)+1)
      print "---------------------------------------------"
      for p in range(2**(lmax+1)+1):
         print nion
         print
         x = pathxyz[p]
         for ii in range(nion):
            print '%s  %8.4f %8.4f %8.4f' % (nwjob['symbols'][ii],x[3*ii]*0.529177,x[3*ii+1]*0.529177,x[3*ii+2]*0.529177)
      print "---------------------------------------------"

   neb_initialize(lmax,timestep)
   neb_freeze_init(lmax)
   neb_set_initial_final(hostsports,nwjob,pathxyz[0],pathxyz[2**(lmax+1)])
   (e0,ef) = neb_get_initial_final_energies()

   print
   print "Intial Bead Energy = %14.6f " % e0
   print "Final Bead Energy  = %14.6f " % ef
   print
   nion3 = 3*nion
   X0 = neb_generate_X(nion3,lmax)
   for p in range(2**(lmax+1)+1):
      for j in range(nion3):
         X0[lmax][j+p*nion3] = pathxyz[p][j]
   for l in range(lmax,0,-1):
      neb_restriction(nion3,l,X0[l],X0[l-1])



   ### started from scratch - build up path from l=0 ###
   plot  = xyplotter.xyplotter(0.0,0.0,1.0,1.0, "neb_relax Plot",2)
   plot2 = xyplotter.xyplotter(0.0,0.0,1.0,1.0, "Residual Errors",3)
   if os.path.isfile(neb_pathsenergies):
      plot_nebpaths(plot,neb_pathsenergies)

  
   if (not readpathxyz):
      K = Kspring/(2.0**(lmax))
      nbeads = 2**(1)+1
      (error,pathenergies,R) = neb_relax(hostsports,nwjob,Nrelax,timestep,nion3,nbeads,0,K,X0[0],[0.0]*nion3*nbeads)
      
      #### plot residual error ####
      plot_nebresidual(plot2,nion3,nbeads,R)

      #### plot nebpaths ####
      pfile = open(neb_pathsenergies,'a')
      for i in range(nbeads):
         pfile.write("%f %f\n" % (float(i)/float(nbeads-1),pathenergies[i]))
      pfile.write("\n")
      pfile.close()
      plot_nebpaths(plot,neb_pathsenergies)


      print "l,error",0,error,pathenergies
      for l in range(lmax):
         nbeads = 2**(l+2)+1
         neb_prolongation(nion3,l,X0[l],X0[l+1])
         K *= 2.0
         (error,pathenergies,R) = neb_relax(hostsports,nwjob,Nrelax,timestep,nion3,nbeads,l+1,K,X0[l+1],[0.0]*nion3*nbeads)
         print "l,error",l+1,error,pathenergies

         #### plot residual error ####
         plot_nebresidual(plot2,nion3,nbeads,R)

         #### plot nebpaths ####
         pfile = open(neb_pathsenergies,'a')
         for i in range(nbeads):
            pfile.write("%f %f\n" % (float(i)/float(nbeads-1),pathenergies[i]))
         pfile.write("\n")
         pfile.close()
         plot_nebpaths(plot,neb_pathsenergies)



   ### perform NEB Vcycle Sweeps ###
   for it in range(Nsweeps):

      nbeads = 2**(lmax+1)+1
      if (Vcycle):
         (error,pathenergies,R) = neb_vcycle_relax(hostsports,nwjob,Nrelax,timestep,nion3,lmax,Kspring,X0)
         print "Vcyle sweeps it,error=",it,error
      else:
         (error,pathenergies,R) = neb_relax(hostsports,nwjob,Nrelax,timestep,nion3,nbeads,lmax,Kspring,X0[lmax],[0.0]*nbeads*nion3)
         print "quasi-newton sweeps it,error=",it,error

      #### plot residual error ####
      plot_nebresidual(plot2,nion3,nbeads,R)

      #### plot nebpaths ####
      pfile = open(neb_pathsenergies,'a')
      for i in range(nbeads):
         pfile.write("%f %f\n" % (float(i)/float(nbeads-1),pathenergies[i]))
      pfile.write("\n")
      pfile.close()
      plot_nebpaths(plot,neb_pathsenergies)


      #### save xyz file of trajectory ####
      nbeads = 2**(lmax+1)+1
      xyzfile = open('nebsweeps.xyz','w')
      for p in range(nbeads):
         xyzfile.write("%d\n\n" % nion)
         for ii in range(nion):
            xyzfile.write("%s  %f %f %f\n" % (nwjob['symbols'][ii], X0[lmax][3*ii+p*nion3]*0.529177, X0[lmax][3*ii+1+p*nion3]*0.529177,X0[lmax][3*ii+2+p*nion3]*0.529177))

      xyzfile.close()





if __name__ == "__main__":
  main()
